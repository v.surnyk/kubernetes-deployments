# kubernetes-deployments

In order to create the App with configured service, deployment, ingress in k8s: 

1. Install kubectl utility
2. Use the following commands to deploy the App with configured service, deployment, ingress in required namespace:

```
kubectl apply -f deployment.yaml -n app
kubectl apply -f service.yaml -n app
kubectl apply -f ingress.yaml -n app
```

3. install nginx-ingress controller :

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm install ingress-nginx ingress-nginx/ingress-nginx
```
4. DNS name http://v.int.itoutposts.com/ was provided for LoadBalancer IP 34.118.50.130

5.  install the cert-manager to update SSL certifiacte automatically

```
helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager --create-namespace \
  --version v1.10.0 \
  --set installCRDs=true

```

6. create letsencrypt-production issuer to provide SSL sertificate for your DNS name

```
kubectl apply -f ClusterIssuer.yaml -n app

```

7. install Flux  https://fluxcd.io/flux/installation/

8. Create and config Flux system repo in the this repo using bootstrap 
https://fluxcd.io/flux/installation/#gitlab-and-gitlab-enterprise

9. create and configure Automation, policy , registry files for DEV, Stage , Prod env

https://fluxcd.io/flux/guides/image-update/

10. configure flux ImageUpdateAutomation to achieve auto deploy for DEV/STAGING/PROD env when new docker image appear in docker registry 

11. Flux notification to Slack was configured 
https://fluxcd.io/flux/guides/notifications/

12. FLux webhook was configured https://fluxcd.io/flux/guides/webhook-receivers/.

